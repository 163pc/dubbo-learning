package com.lagou.homework.impl;

import com.lagou.homework.api.UserService;
import org.apache.dubbo.config.annotation.Service;

/**
 * @author 皮卡小丘丘 2021/2/24
 */
@Service
public class UserServiceImpl implements UserService {
    @Override
    public String methodA(String msg) {
        int time = (int) Math.random() * 100;
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            return "调用methodA:"+msg;
        }
    }

    @Override
    public String methodB(String msg) {
        int time = (int) Math.random() * 100;
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            return "调用methodB:"+msg;
        }
    }

    @Override
    public String methodC(String msg) {
        int time = (int) Math.random() * 100;
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            return "调用methodC:"+msg;
        }
    }
}

package com.lagou.homework;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.IOException;

/**
 * @author 皮卡小丘丘 2021/2/24
 */
public class ProviderMain {
            public static void main(String[] args) throws IOException, InterruptedException {
                AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ProviderConfiguration.class);
                context.start();
                byte[] command =  new byte[1];
                System.in.read(command);
                while(command[0]!='1'){
                    Thread.sleep(500);
                    System.in.read(command);
                }
    }

    @Configuration
    @EnableDubbo(scanBasePackages = {"com.lagou.homework.impl"})
    @PropertySource("classpath:/dubbo-provider.properties")
    static class ProviderConfiguration{
    }
}

package com.lagou.homework.filter;

import com.lagou.homework.utils.TPMonitor;
import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;

/**
 * @author 皮卡小丘丘 2021/2/24
 */
@Activate(group = CommonConstants.CONSUMER)
public class TPMonitorFilter implements Filter {
    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        long beginTime = System.currentTimeMillis();
        try {
            return invoker.invoke(invocation);
        }finally {
            long endTime = System.currentTimeMillis();
            //以方法名区分
            String methodName = invocation.getMethodName();
            TPMonitor.addRecord(methodName, endTime-beginTime,endTime);
        }
    }
}

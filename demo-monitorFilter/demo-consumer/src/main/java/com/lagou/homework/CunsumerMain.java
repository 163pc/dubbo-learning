package com.lagou.homework;

import com.lagou.homework.component.ConsumerRunner;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.concurrent.ExecutionException;

/**
 * @author 皮卡小丘丘 2021/2/24
 */
public class CunsumerMain {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConsumerConfiguration.class);
        ConsumerRunner bean = context.getBean(ConsumerRunner.class);
        bean.start("参数");

    }

    @Configuration
    @ComponentScan(basePackages = {"com.lagou.homework.component"})
    @PropertySource("classpath:/dubbo-consumer.properties")
    @EnableDubbo
    static class ConsumerConfiguration{
    }
}

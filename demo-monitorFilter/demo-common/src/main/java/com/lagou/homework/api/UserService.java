package com.lagou.homework.api;

/**
 * @author 皮卡小丘丘 2021/2/24
 */
public interface UserService {
    String methodA(String msg);
    String methodB(String msg);
    String methodC(String msg);
}
